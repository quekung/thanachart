﻿

function validateSpecialKey(evt, ojbid, valtext) {
    //evt     คือ ตัวอักษรของ key นั้นๆ
    //ojbid   คือ ชื่อ ID Label Alert
    //valtext คือ ข้อความ Alert
    
    var key;
    var ret;
    var checkKey;
    if (window.event) {
        key = window.event.keyCode; // ใช้กับ IE
        ret = "window.event.returnValue = false;";
    } else if (evt) {
        key = evt.which; // ใช้กับ Firefox 
        ret = "keyPressed = evt.preventDefault()";
    }
    
    //SpecialKey <,>,~,^,:,;,%,&,',",{,},[,]
    if (key != 8 && key != 0) {

        if (key == 60 || key == 62 || key == 126 || key == 94 || key == 58 || key == 59 || key == 37 || key == 38 || key == 39 || key == 34 || key == 123 || key == 125 || key == 91 || key == 93) {
            checkKey = true;
        }
        else {
            checkKey = false;
        }
        //alert(checkKey.toString());
        if (checkKey == true) {
            if (ojbid != "") {
                eval(ret);
                document.getElementById(ojbid).innerHTML = "" + valtext;
            }
            else {
                eval(ret);
            }
        }
    }
}

function validateSpecialKeyTHOnly(evt, ojbid, valtext) {
    //evt     คือ ตัวอักษรของ key นั้นๆ
    //ojbid   คือ ชื่อ ID Label Alert
    //valtext คือ ข้อความ Alert

    var key;
    var ret;
    var checkKey;
    if (window.event) {
        key = window.event.keyCode; // ใช้กับ IE
        ret = "window.event.returnValue = false;";
    } else if (evt) {
        key = evt.which; // ใช้กับ Firefox 
        ret = "keyPressed = evt.preventDefault()";
    }
    if (key != 8 && key != 0) {

        //TH หาว่ามี en มั้ยถ้าเจอละตีออก
        if ((key >= 65 && key <= 90) || (key >= 97 && key <= 122)) {
            checkKey = true;
        }
        else {
            checkKey = false;
        }
        //SpecialKey <,>,~,^,:,;,%,&,',",{,},[,]
        if (key == 60 || key == 62 || key == 126 || key == 94 || key == 58 || key == 59 || key == 37 || key == 38 || key == 39 || key == 34 || key == 123 || key == 125 || key == 91 || key == 93) {
            checkKey = true;
        }
        else {
            checkKey = false;
        }

        if (checkKey == true) {
            if (ojbid != "") {
                eval(ret);
                document.getElementById(ojbid).innerHTML = "" + valtext;
            }
            else {
                eval(ret);
            }
        }
    }
}

function validateSpecialKeyENOnly(evt, ojbid, valtext) {
    //evt     คือ ตัวอักษรของ key นั้นๆ
    //ojbid   คือ ชื่อ ID Label Alert
    //valtext คือ ข้อความ Alert

    var key;
    var ret;
    var checkKey;
    if (window.event) {
        key = window.event.keyCode; // ใช้กับ IE
        ret = "window.event.returnValue = false;";
    } else if (evt) {
        key = evt.which; // ใช้กับ Firefox 
        ret = "keyPressed = evt.preventDefault()";
    }

    if (key != 8 && key != 0) {

        //EN หาว่ามี th มั้ยถ้าเจอละตีออก
        if ((key >= 161 && key <= 255) || (key >= 3585 && key <= 3675)) {
            checkKey = true;
        }
        else {
            checkKey = false;
        }
              
        //SpecialKey <,>,~,^,:,;,%,&,',",{,},[,]
        if (key == 60 || key == 62 || key == 126 || key == 94 || key == 58 || key == 59 || key == 37 || key == 38 || key == 39 || key == 34 || key == 123 || key == 125 || key == 91 || key == 93) {
            checkKey = true;
        }
        else {
            checkKey = false;
        }

        if (checkKey == true) {
            if (ojbid != "") {
                eval(ret);
                document.getElementById(ojbid).innerHTML = "" + valtext;
            }
            else {
                eval(ret);
            }
        }
    }
}